{
  description = "Osquery flake";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-22.11;

  outputs = {
    self,
    nixpkgs,
  }: {
    defaultPackage.x86_64-linux =
      # Notice the reference to nixpkgs here.
      with import nixpkgs {system = "x86_64-linux";};
        stdenv.mkDerivation rec {
          name = "osquery-${version}";
          version = "4.9.0_1";

          # this is what `osquery --help` will show as the version.
          OSQUERY_BUILD_VERSION = version;
          OSQUERY_PLATFORM = "NixOS;";

          src = fetchurl {
            url = "https://pkg.osquery.io/linux/osquery-${version}.linux_x86_64.tar.gz";
            sha256 = "sha256-QYf1t24hzpb3ZeUJ+WywRwjENZa3YJ66DloQq1/fWMU=";
          };

          sourceRoot = ".";
          #dontConfigure = true;
          #dontBuild = true;
          nativeBuildInputs = [
            autoPatchelfHook
            zlib
          ];
          propagatedBuildInputs = [jq alejandra];
          installPhase = ''
            ls -la
            mkdir -p $out
            cp -r .  $out
            ls -la $out
          '';
          meta = with lib; {
            description = "SQL powered operating system instrumentation, monitoring, and analytics";
            homepage = https://osquery.io/;
            license = licenses.bsd3;
            platforms = platforms.linux;
            maintainers = with maintainers; [cstrahan ma27];
          };
        };
  };
}
